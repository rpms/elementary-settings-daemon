## START: Set by rpmautospec
## (rpmautospec version 0.2.6)
%define autorelease(e:s:pb:) %{?-p:0.}%{lua:
    release_number = 1;
    base_release_number = tonumber(rpm.expand("%{?-b*}%{!?-b:1}"));
    print(release_number + base_release_number - 1);
}%{?-e:.%{-e*}}%{?-s:.%{-s*}}%{?dist}
## END: Set by rpmautospec

%global srcname settings-daemon
%global appname io.elementary.settings-daemon
%global iface   io.elementary.SettingsDaemon.AccountsService

Name:           elementary-settings-daemon
Version:        1.2.0
Release:        %autorelease
Summary:        Settings Daemon and Portal for Pantheon
License:        GPLv3+

URL:            https://github.com/elementary/settings-daemon
Source0:        %{url}/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  libappstream-glib
BuildRequires:  meson
BuildRequires:  systemd-rpm-macros
BuildRequires:  vala

BuildRequires:  pkgconfig(accountsservice)
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(granite) >= 5.3.0
BuildRequires:  pkgconfig(libgeoclue-2.0)
BuildRequires:  pkgconfig(systemd)

Requires:       xdg-desktop-portal

%description
%{summary}.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%meson
%meson_build


%install
%meson_install


%check
desktop-file-validate \
    %{buildroot}/%{_sysconfdir}/xdg/autostart/%{appname}.desktop

appstream-util validate-relax --nonet \
    %{buildroot}/%{_datadir}/metainfo/%{appname}.appdata.xml


%post
%systemd_user_post %{appname}.xdg-desktop-portal.service


%preun
%systemd_user_preun %{appname}.xdg-desktop-portal.service


%files
%license LICENSE
%doc README.md

%config(noreplace) %{_sysconfdir}/xdg/autostart/%{appname}.desktop

%{_bindir}/%{appname}

%{_libexecdir}/%{appname}.xdg-desktop-portal

%{_datadir}/accountsservice/interfaces/%{iface}.xml
%{_datadir}/dbus-1/interfaces/%{iface}.xml
%{_datadir}/dbus-1/services/org.freedesktop.impl.portal.desktop.elementary.settings-daemon.service
%{_datadir}/glib-2.0/schemas/%{appname}.gschema.xml
%{_datadir}/metainfo/%{appname}.appdata.xml
%{_datadir}/xdg-desktop-portal/portals/%{appname}.portal

%{_userunitdir}/%{appname}.xdg-desktop-portal.service


%changelog
* Tue Jun 07 2022 Fabio Valentini <decathorpe@gmail.com> 1.2.0-1
- Update to version 1.2.0; Fixes RHBZ#2087715

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> 1.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Sat Oct 30 2021 Fabio Valentini <decathorpe@gmail.com> 1.1.0-1
- Update to version 1.1.0; Fixes RHBZ#2018124

* Wed Jul 21 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Fri Jul 16 2021 Fabio Valentini <decathorpe@gmail.com> - 1.0.0-1
- Update to version 1.0.0.

* Mon Jun 14 2021 Fabio Valentini <decathorpe@gmail.com> - 1.0.0-0.2.20210608git730f37a
- Bump to commit 730f37a.

* Sat Feb 20 2021 Fabio Valentini <decathorpe@gmail.com> - 1.0.0-0.1.20210219git7c8d748
- Initial package for Fedora.

